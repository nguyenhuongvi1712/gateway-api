const { default: axios } = require('axios')
const express = require('express')
const app = express()
const PORT = 3001
const route = require('./routes')
app.use(express.json())

app.use(route)
app.listen(PORT, () => {
  console.log('API gateway has started on ' + PORT)
})
