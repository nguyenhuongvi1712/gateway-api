const express = require('express')
const router = express.Router()
const axios = require('axios')
const registry = require('./registry.json')
const fs = require('fs')

const VERIFY_PATH = 'http://localhost:9000/auth/verifyToken'

router.all('/:apiName/:path', async (req, res) => {
  if (registry.services[req.params.apiName]) {
    try {
      if (registry.services[req.params.apiName].checkAuth) {
        const token = req.headers.authorization.split(' ')[1]
        const result = await auth(token)
        if (result.success)
          axios({
            method: req.method,
            url: registry.services[req.params.apiName].url + req.params.path,
            headers: req.headers,
            data: req.body,
          }).then((respone) => {
            res.send(respone.data)
          })
        else res.send('Authentication unsuccessful')
      } else {
        axios({
          method: req.method,
          url: registry.services[req.params.apiName].url + req.params.path,
          headers: req.headers,
          data: req.body,
        }).then((respone) => {
          res.send(respone.data)
        })
      }
    } catch (error) {
      console.log(error)
    }
  } else res.send("API name : '" + req.params.apiName + "' does not exist !\n")
})
router.all('/:apiName', async (req, res) => {
  if (registry.services[req.params.apiName]) {
    try {
      if (registry.services[req.params.apiName].checkAuth) {
        const token = req.headers.authorization.split(' ')[1]
        console.log(token)
        const result = await auth(token)
        console.log(result)
        if (result.success)
          axios({
            method: req.method,
            url: registry.services[req.params.apiName].url,
            headers: req.headers,
            data: req.body,
          }).then((respone) => {
            res.send(respone.data)
          })
        else res.send('Authentication unsuccessful')
      } else {
        axios({
          method: req.method,
          url: registry.services[req.params.apiName].url,
          headers: req.headers,
          data: req.body,
        }).then((respone) => {
          res.send(respone.data)
        })
      }
    } catch (error) {
      console.log(error)
    }
  } else res.send("API name : '" + req.params.apiName + "' does not exist !\n")
})

router.post('/register', (req, res, next) => {
  const registrationInfo = req.body
  registry.services[registrationInfo.apiName] = { ...registrationInfo }
  fs.writeFile('./routes/registry.json', JSON.stringify(registry), (err) => {
    if (err) {
      res.send('Could not register ' + registrationInfo.apiName + '\n' + err)
    } else {
      res.send('Successfully registed !')
    }
  })
})

const auth = async (token) => {
  try {
    const res = await axios.get(VERIFY_PATH, {
      headers: {
        Authorization: `bearer ${token}`,
      },
    })
    console.log(res.data)
    if (res.data.success)
      return {
        success: true,
      }
    else
      return {
        success: false,
      }
  } catch (error) {
    return {
      success: false,
    }
    console.log(error)
  }
}
module.exports = router
